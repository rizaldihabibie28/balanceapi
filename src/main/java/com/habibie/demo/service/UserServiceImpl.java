/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.service;

import com.habibie.demo.model.User;
import com.habibie.demo.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author guest
 */
@Service
public class UserServiceImpl implements UserService  {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Override
    public User save(User user){
        try{
            if(user.getPassword() != null){
                user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            }
            return userRepository.save(user);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    @Override
    public User findUser(int id){
        try{
            return userRepository.findById(id);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    @Override
    public User findUser(String username){
        try{
            return userRepository.findByUsername(username);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    @Override
    public User login(String username, String password){
        try{
            password = (bCryptPasswordEncoder.encode(password));
            return userRepository.findByUsernameAndPassword(username, password);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    @Override
    public List<User> checkExistingUser(String username, String email){
        try{
            return userRepository.findByUsernameOrEmail(username, email);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try{
            User user = userRepository.findByUsername(username);
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),true, true, true, true, new ArrayList<>());
        }catch(Exception e){
            System.out.println(e);
            return null;
        }    
    }
}
