/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.service;

import com.habibie.demo.model.UserBalanceHistory;
import com.habibie.demo.repository.UserBalanceHistoryRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author guest
 */
@Service
@Transactional
public class UserBalanceHistoryService {
    @Autowired
    UserBalanceHistoryRepository userBalanceHistoryRepository;
    
    public boolean save(UserBalanceHistory userBalanceHistory){
        try{
            userBalanceHistoryRepository.save(userBalanceHistory);
            return true;
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
}
