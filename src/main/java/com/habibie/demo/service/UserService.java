/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.service;

import com.habibie.demo.model.User;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author guest
 */
public interface UserService extends UserDetailsService {
    public User save(User user);
    public User findUser(int id);
    public User findUser(String username);
    public User login(String username, String password);
    public List<User> checkExistingUser(String username, String email);
}
