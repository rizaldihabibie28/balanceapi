/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.service;

import com.habibie.demo.model.UserBalance;
import com.habibie.demo.repository.UserBalanceRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author guest
 */
@Service
@Transactional
public class UserBalanceService {
    @Autowired
    UserBalanceRepository userBalanceRepository;
    
    public UserBalance save(UserBalance userBalance){
        try{
            return userBalanceRepository.save(userBalance);
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    public UserBalance getCurrentBalance(int userId){
        return userBalanceRepository.findByUserId(userId);
    }
    
}
