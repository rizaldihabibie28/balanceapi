/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.controller;

import com.habibie.demo.model.CustomResponse;
import com.habibie.demo.model.TransferDTO;
import com.habibie.demo.model.User;
import com.habibie.demo.model.UserBalance;
import com.habibie.demo.model.UserBalanceHistory;
import com.habibie.demo.service.UserBalanceHistoryService;
import com.habibie.demo.service.UserBalanceService;
import com.habibie.demo.service.UserServiceImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author guest
 */
@RestController
@RequestMapping("demo")
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserBalanceService userBalanceService;
    
    @Autowired
    private UserBalanceHistoryService userBalanceHistoryService;
    @PostMapping(path = "/signup",consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity signup(@Valid @RequestBody User user){
        CustomResponse customResponse= new CustomResponse();
        List<User> existingUser = userService.checkExistingUser(user.getUsername(), user.getEmail());
        if(existingUser != null && existingUser.size() > 0){
            customResponse.setCode(HttpStatus.CONFLICT);
            customResponse.setMessage("Data already exist");
        }else{
            User newUser = userService.save(user);
            if(newUser != null){
                customResponse.setCode(HttpStatus.CREATED);
                customResponse.setMessage("Created");
            }else{
                customResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
                customResponse.setMessage("Failed to create user");            
            }            
        }
        return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }
//    @PostMapping(path = "/login",consumes = {MediaType.APPLICATION_JSON_VALUE},
//            produces = {MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity login(@RequestBody User user){
//        User userLogin = userService.login(user.getUsername(), user.getPassword());
//        CustomResponse customResponse= new CustomResponse();
//        
//        if(userLogin == null){
//            customResponse.setCode(HttpStatus.NOT_FOUND);
//            customResponse.setMessage("Wrong username or password");
//        }else{
//            if(userLogin.isLoggedIn()){
//                customResponse.setCode(HttpStatus.OK);            
//                customResponse.setMessage("Already Logged In");
//            }else{
//                customResponse.setCode(HttpStatus.OK);            
//                customResponse.setMessage("Success");
//                customResponse.setData(userLogin);
//                userLogin.setIsLoggedIn(true);
//                userService.save(userLogin);
//            }
//        }
//        return new ResponseEntity<>(customResponse, HttpStatus.OK);
//    }
    
    @PostMapping(path = "/logout",consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity logout(@RequestBody User user){
        User userLogin = userService.login(user.getUsername(), user.getPassword());
        CustomResponse customResponse= new CustomResponse();
        if(userLogin == null){
            customResponse.setCode(HttpStatus.NOT_FOUND);
            customResponse.setMessage("Wrong username or password");
        }else{
            if(!userLogin.isLoggedIn()){
                customResponse.setCode(HttpStatus.OK);            
                customResponse.setMessage("Already Logged Out");
            }else{
                customResponse.setCode(HttpStatus.OK);            
                customResponse.setMessage("Success");
                userLogin.setIsLoggedIn(false);
                userService.save(userLogin);
                
            }
        }
        return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }
    @PostMapping(path = "/debit",consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity debit(@RequestBody UserBalance userBalance, HttpServletRequest request){
        return this.debitAction(userBalance, request, "setoran");
    }
    
    @PostMapping(path = "/credit",consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity credit(@RequestBody UserBalance userBalance, HttpServletRequest request){
        return this.creditAction(userBalance, request, "tarik");
    }
    
    @PostMapping(path = "/transfer",consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity transfer(@RequestBody TransferDTO transferDto,  HttpServletRequest request){
        CustomResponse customResponse= new CustomResponse();
        if(transferDto.getSourceId() != 0 && transferDto.getDestinationId() != 0){
            UserBalance sourceBalance = userBalanceService.getCurrentBalance(transferDto.getSourceId());
            User destinationUser = userService.findUser(transferDto.getDestinationId());
            if(destinationUser == null){
                customResponse.setCode(HttpStatus.EXPECTATION_FAILED);            
                customResponse.setMessage("Destination Not Found");
                return new ResponseEntity<>(customResponse, HttpStatus.NOT_ACCEPTABLE);
            }
            int currentSourceBalance = 0;
            int sourceBalanceBefore = 0;
            if(sourceBalance != null){
                currentSourceBalance = sourceBalance.getBalance();
                sourceBalanceBefore = currentSourceBalance;
                currentSourceBalance = currentSourceBalance-transferDto.getBalance();
                if(currentSourceBalance >= 0){
                    sourceBalance.setBalance(currentSourceBalance);
                    UserBalanceHistory userBalanceHistory = new UserBalanceHistory();
                    userBalanceHistory.setBalanceBefore(sourceBalanceBefore);
                    userBalanceHistory.setBalanceAfter(currentSourceBalance);
                    userBalanceHistory.setActivity("transfer");
                    userBalanceHistory.setType("kredit");
                    userBalanceHistory.setIp(request.getRemoteAddr());
                    userBalanceHistory.setUserBalance(sourceBalance);
                    String userAgent = request.getHeader("User-Agent");
                    userBalanceHistory.setUserAgent(userAgent);
                    List<UserBalanceHistory> histories = new ArrayList<>();
                    histories.add(userBalanceHistory);
                    sourceBalance.setHistories(histories);
                    sourceBalance = userBalanceService.save(sourceBalance);
                    if(sourceBalance != null && sourceBalance.getId() > 0){
                        UserBalance destinationBalance = new UserBalance();
                        destinationBalance.setUserId(transferDto.getDestinationId());
                        destinationBalance.setBalance(transferDto.getBalance());
                        return this.debitAction(destinationBalance, request, "debit");
                    }else{
                        customResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR);            
                        customResponse.setMessage("Failed to update balance");                    
                    }
                    return new ResponseEntity<>(customResponse, HttpStatus.OK);
                }else{
                    customResponse.setCode(HttpStatus.EXPECTATION_FAILED);            
                    customResponse.setMessage("Balance Is Not Enough");
                    return new ResponseEntity<>(customResponse, HttpStatus.NOT_ACCEPTABLE);
                }                
            }else{
                customResponse.setCode(HttpStatus.EXPECTATION_FAILED);            
                customResponse.setMessage("Balance Is Not Enough");
                return new ResponseEntity<>(customResponse, HttpStatus.NOT_ACCEPTABLE);
            }
        }else{
            customResponse.setCode(HttpStatus.BAD_REQUEST);            
            customResponse.setMessage("Bad Request");
            return new ResponseEntity<>(customResponse, HttpStatus.BAD_REQUEST);
        }
    }
    private ResponseEntity creditAction(UserBalance userBalance, HttpServletRequest request, String activity){
        CustomResponse customResponse= new CustomResponse();
        if(userBalance.getUserId() != 0 && userBalance.getBalance() != 0){
            UserBalance userBalanceDebit = userBalanceService.getCurrentBalance(userBalance.getUserId());
            int currentBalance = 0;
            int balanceBefore = 0;
            if(userBalanceDebit != null){
                currentBalance = userBalanceDebit.getBalance();
                balanceBefore = currentBalance;
            }else{
                userBalanceDebit = new UserBalance();
            }
            currentBalance = currentBalance-userBalance.getBalance();
            if(currentBalance >= 0){
                userBalanceDebit.setBalance(currentBalance);
                UserBalanceHistory userBalanceHistory = new UserBalanceHistory();
                userBalanceHistory.setUserBalance(userBalanceDebit);
                userBalanceHistory.setBalanceBefore(balanceBefore);
                userBalanceHistory.setBalanceAfter(currentBalance);
                userBalanceHistory.setActivity(activity);
                userBalanceHistory.setType("kredit");
                userBalanceHistory.setIp(request.getRemoteAddr());
                String userAgent = request.getHeader("User-Agent");
                userBalanceHistory.setUserAgent(userAgent);
                List<UserBalanceHistory> histories = new ArrayList<>();
                histories.add(userBalanceHistory);
                userBalanceDebit.setHistories(histories);
                userBalanceDebit = userBalanceService.save(userBalanceDebit);
                if(userBalanceDebit != null && userBalanceDebit.getId() > 0){
                    customResponse.setCode(HttpStatus.OK);            
                    customResponse.setMessage("Success");                    
                }else{
                    customResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR);            
                    customResponse.setMessage("Failed to update balance");                    
                }
                return new ResponseEntity<>(customResponse, HttpStatus.OK);
            }else{
                customResponse.setCode(HttpStatus.EXPECTATION_FAILED);            
                customResponse.setMessage("Balance Is Not Enough");
                return new ResponseEntity<>(customResponse, HttpStatus.NOT_ACCEPTABLE);
            }
        }else{
            customResponse.setCode(HttpStatus.BAD_REQUEST);            
            customResponse.setMessage("Bad Request");
            return new ResponseEntity<>(customResponse, HttpStatus.BAD_REQUEST);
        }
    }
    private ResponseEntity debitAction(UserBalance userBalance, HttpServletRequest request, String activity){
        CustomResponse customResponse= new CustomResponse();
        if(userBalance.getUserId() != 0 && userBalance.getBalance() != 0){
            UserBalance userBalanceDebit = userBalanceService.getCurrentBalance(userBalance.getUserId());
            int currentBalance = 0;
            int balanceBefore = -0;
            if(userBalanceDebit != null){
                currentBalance = userBalanceDebit.getBalance();
                balanceBefore = currentBalance;
            }else{
                userBalanceDebit = new UserBalance();
                userBalanceDebit.setUserId(userBalance.getUserId());
            }
            currentBalance = currentBalance+userBalance.getBalance();
            userBalanceDebit.setBalance(currentBalance);
            UserBalanceHistory userBalanceHistory = new UserBalanceHistory();
            userBalanceHistory.setUserBalance(userBalanceDebit);
            userBalanceHistory.setBalanceBefore(balanceBefore);
            userBalanceHistory.setBalanceAfter(currentBalance);
            userBalanceHistory.setActivity(activity);
            userBalanceHistory.setType("debit");
            userBalanceHistory.setIp(request.getRemoteAddr());
            String userAgent = request.getHeader("User-Agent");
            userBalanceHistory.setUserAgent(userAgent);
            List<UserBalanceHistory> histories = new ArrayList<>();
            histories.add(userBalanceHistory);
            userBalanceDebit.setHistories(histories);
            userBalanceDebit = userBalanceService.save(userBalanceDebit);
            if(userBalanceDebit != null && userBalanceDebit.getId() > 0){
                customResponse.setCode(HttpStatus.OK);            
                customResponse.setMessage("Success");                    
            }else{
                customResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR);            
                customResponse.setMessage("Failed to update balance");                    
            }
            return new ResponseEntity<>(customResponse, HttpStatus.OK);
        }else{
            customResponse.setCode(HttpStatus.BAD_REQUEST);            
            customResponse.setMessage("Bad Request");
            return new ResponseEntity<>(customResponse, HttpStatus.BAD_REQUEST);
        }
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public CustomResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
        CustomResponse errors = new CustomResponse();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.setCode(HttpStatus.BAD_REQUEST);
            errors.setMessage(errorMessage);
        });
        return errors;
    }
}
