/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.repository;

import com.habibie.demo.model.UserBalance;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author guest
 */
@Repository
public interface UserBalanceRepository extends JpaRepository<UserBalance, Long>{
    UserBalance findByUserId(int userId);
}
