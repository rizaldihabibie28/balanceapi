/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.model;

import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author guest
 */
@Entity
@Table(name="user_balance")
public class UserBalance {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name="userId")
    private int userId;
    
    @Column(name="balance")
    private int balance;
    
    @Column(name="balanceArchieve")
    private int balanceArchieve;
    
    @OneToMany(mappedBy="userBalance",cascade = CascadeType.ALL)
    private List<UserBalanceHistory> histories;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBalanceArchieve() {
        return balanceArchieve;
    }

    public void setBalanceArchieve(int balanceArchieve) {
        this.balanceArchieve = balanceArchieve;
    }

    public List<UserBalanceHistory> getHistories() {
        return histories;
    }

    public void setHistories(List<UserBalanceHistory> histories) {
        this.histories = histories;
    }
    
}
