/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.habibie.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author guest
 */
@Entity
@Table(name="user_balance_history")
public class UserBalanceHistory {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne(cascade=CascadeType.ALL,optional = false)
    @JoinColumn(name="userBalanceId")
    private UserBalance userBalance;
    
    @Column(name="balanceBefore")
    private int balanceBefore;
    
    @Column(name="balanceAfter")
    private int balanceAfter;
    
    @Column(name="activity")
    private String activity;
    
    @Column(name="type")
    private String type;
    
    @Column(name="ip")
    private String ip;
    
    @Column(name="location")
    private String location;
    
    @Column(name="userAgent")
    private String userAgent;
    
    @Column(name="author")
    private String author;

    public UserBalance getUserBalance() {
        return userBalance;
    }

//    public int getUserBalanceId() {
//        return userBalanceId;
//    }
//
//    public void setUserBalanceId(int userBalanceId) {
//        this.userBalanceId = userBalanceId;
    public void setUserBalance(UserBalance userBalance) {
        this.userBalance = userBalance;
    }

//    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(int balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public int getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(int balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
