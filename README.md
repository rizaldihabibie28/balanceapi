Demo application for Pretest at PrivyID
Framework   : Springboot v2.4.5
Database    : MySQL 8
ORM         : JPA + Hibernate
Built Tool  : Maven
Java        : 8
Cara menjalankan program : 
A. Lewat Editor Netbeans
1. Import project ke dalam Editor
2. klik kanan project->run maven->goals. pada field Goals isi dengan clean install -DSkipTests
3. Klik Ok
4. Klik kanan project->set configuration->customize. Pilih Springboot pada tab categories. Pada field application arguments isi dengan --spring.config.location=/path/to/field. klik OK
5. Klik kanan project->run
B. Cara Menjalankan jar file
1. buka folder yang berisi file jar
2. masuk ke terminal
3. masukkan command java -jar demo-0.0.1-SNAPSHOT.jar --spring.config.location=/path/to/file

Application base url : http://localhost:8080/demo
Format request body signup ("/signup"):
{"username":"username","email":"email@email.com","password":"password"}

Format request body signup ("/login"):
{"username":"email","password":"password"}

Format request body debit ("/debit"):
{"userId":"1","balance":2000}

Format request body debit ("/credit"):
{"userId":"1","balance":2000}

Format request body transfer ("/transfer"):
{"sourceId":"2","destinationId":"1","balance":1000}


